export const createReducer = (initialState, handlers) => {
    let createdHandlers
    if (typeof handlers === 'function') createdHandlers = handlers()
    else createdHandlers = handlers
    return function reducer(state=initialState, action) {
        if (createdHandlers.hasOwnProperty(action.type)) {
            return createdHandlers[action.type](state, action)
        } else {
            return state
        }
    }
}