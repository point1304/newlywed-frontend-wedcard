import camelize from 'camelize'

export const callAPI = async (endpoint, option) => {
    const res = await fetch(endpoint, option)
    if (!res.ok) {
        const err = await res.json()
        if (!err.status) err.status = res.status
        return camelize(err)
    }
    const ret = await res.json()
    return camelize(ret)
}

export const postAPI = async (endpoint, body) => {
    const res = await fetch(endpoint, {
        method     : 'POST',
        headers    : {'Content-Type': 'application/json'},
        body
    })
    if (!res.ok) {
        const err = await res.json()
        if (!err.status) err.status = res.status
        return camelize(err)
    }
    const ret = await res.json()
    return camelize(ret)
}

export const deleteAPI = async (endpoint, body) => {
    const res = await fetch(endpoint, {
        method: 'DELETE',
        headers: {'Content-Type': 'application/json'},
        body
    })
    if (!res.ok) {
        const err = await res.json()
        if (!err.status) err.status = res.status
        return camelize(err)
    }
    const ret = await res.json()
    return camelize(ret)
}
