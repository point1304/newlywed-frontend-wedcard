import { call, put } from 'redux-saga/effects'
import isArray from 'lodash/isArray'

export function* apiSaga(api, asyncAction, options) {
    let apiArgs, actionPayload
    if (options) {
        apiArgs = options.apiArgs
        actionPayload = options.actionPayload
    }
    try {
        const result = yield isArray(apiArgs) ?
                                call(api, ...apiArgs) :
                                call(api, apiArgs)
        if (result.error) {
            yield put(asyncAction.fail({data: result, ...actionPayload}))
            return false
        } else {
            yield put(asyncAction.success({data: result, ...actionPayload}))
            return true
        }
    } catch(err) {
        const error = {error: err.name, message: err.message}
        yield put(asyncAction.fail({data: error, ...actionPayload}))
        return false
    }
}