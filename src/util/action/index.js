import { keyMirror } from '..'

export function makeAsyncActions(actionNames) {
    const prefix = actionNames
    return keyMirror(prefix, {
        INDEX: null,
        REQUEST: null,
        SUCCESS: null,
        FAIL: null,
    })
}

export function makeActionCreator(actionTypes) {
    return payload => ({ type: actionTypes, payload })
}

export function makeAsyncActionCreator(actions) {
    const ret = makeActionCreator(actions.INDEX)
    ret.request = makeActionCreator(actions.REQUEST)
    ret.success = makeActionCreator(actions.SUCCESS)
    ret.fail = makeActionCreator(actions.FAIL)
    return ret
}