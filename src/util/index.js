import reduce from 'lodash/reduce'

export const keyMirror = (prefix, obj) => {
    const ret = {};
    if (!obj instanceof Object) {
        throw Error('keyMirror(...): Second argument must be an object');
    }
    for (let key in obj) {
        if (!obj.hasOwnProperty(key)) {
            continue;
        }
        ret[key] = `${prefix}_${key}`;
    }
    return ret;
}

export const toISOStringWithTz = (dateObj) => {
    const tzo = -dateObj.getTimezoneOffset(),
        dif = tzo >= 0 ? '+' : '-',
        pad = (num) => {
            const norm = Math.floor(Math.abs(num))
            return (norm < 10 ? '0' : '') + norm
        }
    return dateObj.getFullYear() +
        '-' + pad(dateObj.getMonth() + 1) +
        '-' + pad(dateObj.getDate()) +
        'T' + pad(dateObj.getHours()) +
        ':' + pad(dateObj.getMinutes()) +
        ':' + pad(dateObj.getSeconds()) +
        dif + pad(tzo / 60) +
        ':' + pad(tzo % 60)
}

export const getRandomInt = (min, max) => {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min)) + min
}

const encodeURIComponentPlus = uri => (
    encodeURIComponent(uri).replace(/%20/g, '+')
)

export const urlencode = mapping => (
    reduce(mapping, (r, v, k) => (
        `${r}${encodeURIComponentPlus(k)}=${encodeURIComponentPlus(v)}&`
    ), '').slice(0, -1)
)

export const quoteSearchParams = mapping => (
    encodeURIComponent(
        reduce(mapping, (r, v, k) => (
        `${r}${k}=${v}:`
        ), '')
    ).replace(/%20/g, '+')
)
export const loadScript = ({src, id}, callback) => {
    const isLoaded = document.getElementById(id)
    if (!isLoaded) {
        const script = document.createElement('script')
        script.src = src
        script.id = id
        document.body.appendChild(script)
        script.onload = () => {
            if (callback) callback()
        }
    }
    if (isLoaded && callback) callback()
}

export const timeSince = ts => {
    const now = new Date(),
        secPast = (now.getTime() - ts) / 1000

    if (secPast < 60) {
        return parseInt(secPast) + '초 전'
    }
    else if (secPast < 3600) {
        return parseInt(secPast / 60) + '분 전'
    }
    else if (secPast <= 86400) {
        return parseInt(secPast / 3600) + '시간 전'
    }
    else if (secPast > 86400) {
        return new Date(ts).toDateString()
    }
}