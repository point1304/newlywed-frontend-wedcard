import { combineReducers } from 'redux'
import { weddingInfo } from './page/MainPage/reducer'
import { comments, isEditingComment, commentTo, commentToDelete } from './container/Comment/reducer'

const rootReducer = combineReducers({
    weddingInfo,
    comments,
    isEditingComment,
    commentTo,
    commentToDelete,
});
export default rootReducer