import { call, put, takeLeading } from 'redux-saga/effects'

import { actionTypes, postCommentAction, postReplyAction } from './action'
import { cancelCreateCommentAction } from '../Comment/action'
import { postComment, postReply } from '../../api'
import { apiSaga } from '../../util/saga'

function* postCommentSaga(action) {
    const success = yield call(
        apiSaga,
        postComment,
        postCommentAction,
        { apiArgs: action.payload.body }
    )
    if (success) {
        yield put(cancelCreateCommentAction())
    }
}
function* postReplySaga(action) {
    const { commentId, body } = action.payload
    const success = yield call(
        apiSaga,
        postReply,
        postReplyAction,
        {
            apiArgs: [commentId, body],
        }
    )
    if (success) {
        yield put(cancelCreateCommentAction())
    }
}
function* commentFormSaga() {
    yield takeLeading(actionTypes.POST_COMMENT.REQUEST, postCommentSaga)
    yield takeLeading(actionTypes.POST_REPLY.REQUEST, postReplySaga)
}
export default commentFormSaga