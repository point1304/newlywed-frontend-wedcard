import React from 'react'
import { connect } from 'react-redux'
import FixedForm from '../../component/FixedForm'
import { Input, TextAreaAndSubmit } from '../../component/Form'

import { postCommentAction, postReplyAction } from './action'

import each from 'lodash/each'

const schema = {
    name: {
        title: '이름',
        type: 'text',
        component: Input,
        // lookahead regex doesn't work in most of the browsers.
        // so, compromised with the best available regex.
        pattern: /^[^\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,\-.\/:;<=>?@\[\]^_`{|}~\s](([^\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,\-.\/:;<=>?@\[\]^_`{|}~\s]|[-_ ])(?!\s{2,})){0,18}[^\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,\-.\/:;<=>?@\[\]^_`{|}~\s]$/,
        maxLength: 30,
        onChange: e => {
            const value = e.currentTarget.value
                            .replace(/\s+/g, ' ')
                            .replace(/^[\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,\-.\/:;<=>?@\[\]^_`{|}~\s]/, '')
                            .replace(/[\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,\.\/:;<=>?@\[\]^`{|}~]/g, '')
            e.currentTarget.value = value
        },
        required: true
    },
    password: {
        title: '비밀번호',
        type: 'password',
        component: Input,
        pattern: /^[a-zA-Z0-9 !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{4,50}$/,
        maxLength: 50,
        onChange: e => {
            if (/\s+/.test(e.currentTarget.value)) {
                alert('비밀번호에 공백문자를 포함할 수 없습니다.')
                e.currentTarget.value = ''
            }
        },
        required: true
    },
    text: {
        title: '메시지',
        type: 'textarea',
        component: TextAreaAndSubmit,
        minLength: 2,
        maxLength: 1000,
        required: true
    }
}
const order = ['name', 'password', 'text']

const CommentForm = props => {
    return (
        <FixedForm
            show={props.isEditingComment}
            schema={schema}
            order={order}
            onSubmit={props.handleSubmit}
            commentTo={props.commentTo}
        />
    )
}
const mapStateToProps = state => {
    return {
        isEditingComment: state.isEditingComment,
        commentId: state.commentTo,
        commentTo: state.commentTo !== null ?
                    state.comments.byId[state.commentTo].name :
                    '신랑 & 신부'
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        handlePostComment: body => {
            dispatch((postCommentAction.request({ body })))
        },
        handlePostReply: (commentId, body) => {
            dispatch((postReplyAction.request({ commentId, body })))
        }
    }
}
const mergeProps = (stateProps, dispatchProps) => {
    return {
        ...stateProps,
        ...dispatchProps,
        handleSubmit: e => {
            const formData = {}
            each(e.currentTarget.querySelectorAll('input,textarea'), el => {
                if (el.type !== 'submit') {
                    formData[el.name] = el.value
                }
            })
            const body = JSON.stringify(formData)
            if (stateProps.commentId === null) {
                dispatchProps.handlePostComment(body)
            }
            else {
                dispatchProps.handlePostReply(stateProps.commentId, body)
            }
            e.currentTarget.reset()
        }
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(CommentForm)
