import { makeAsyncActions, makeAsyncActionCreator } from '../../util/action'

const PREFIX = '/COMMENT_FORM'

export const actionTypes = {
    POST_COMMENT: makeAsyncActions(`${PREFIX}/POST_COMMENT`),
    POST_REPLY: makeAsyncActions(`${PREFIX}/POST_REPLY`),
}
export const postCommentAction = makeAsyncActionCreator(actionTypes.POST_COMMENT)
export const postReplyAction = makeAsyncActionCreator(actionTypes.POST_REPLY)