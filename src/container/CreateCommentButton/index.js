import React from 'react'
import { connect } from 'react-redux'
import { createCommentAction } from '../Comment/action'
import styles from './index.module.scss'

const CreateCommentButton = props => {
    return (
        <div
            className={styles._}
            onClick={props.handleClick}
        >
            메시지 작성
        </div> 
    )
}
const mapDispatchToProps = dispatch => {
    return {
        handleClick: e => {
            e.stopPropagation()
            dispatch(createCommentAction())
        }
    }
}
export default connect(
    null,
    mapDispatchToProps,
)(CreateCommentButton)