import { combineReducers } from 'redux'
import { actionTypes } from './action'
import { actionTypes as commentFormActionTypes } from '../CommentForm/action'
import { createReducer } from '../../util/reducer'

const endId = (state=null, action) => {
    switch (action.type) {
        case actionTypes.FETCH_COMMENTS.SUCCESS:
            return action.payload.data.result.endId
        default:
            return state
    }
}
const count = (state=0, action) => {
    switch (action.type) {
        case actionTypes.FETCH_COMMENTS.SUCCESS:
            return action.payload.data.result.count
        default:
            return state
    }
}
const byId = createReducer({}, () => {
    let handleFetchCommentsSuccess = (state, action) => ({
        ...state,
        ...action.payload.data.entities.comments
    })
    return {
        [actionTypes.FETCH_COMMENTS.SUCCESS]: handleFetchCommentsSuccess,
        [commentFormActionTypes.POST_COMMENT.SUCCESS]: handleFetchCommentsSuccess,
        [commentFormActionTypes.POST_REPLY.SUCCESS]: (state, action) => {
            const commentId = action.payload.data.result
            const parentId  = action.payload.data.entities.comments[commentId].parent
            return {
                ...state,
                ...action.payload.data.entities.comments,
                [parentId]: {
                    ...state[parentId],
                    children: {
                        ...state[parentId].children,
                        data: [
                            ...state[parentId].children.data,
                            commentId
                        ]
                    }
                }
            }
        },
        [actionTypes.DELETE_COMMENT.SUCCESS]: (state, action) => {
            const commentId = action.payload.commentId
            const newState = {...state}
            const parent = newState[commentId].parent
            if (parent) {
                const idx = newState[parent].children.data.indexOf(commentId)
                newState[parent].children.data.splice(idx, 1)
            }
            delete newState[commentId]
            return newState
        },
        [actionTypes.FETCH_REPLIES]: (state, action) => {
            const {parentId, data: {result, entities}} = action.payload
            return {
                ...state,
                ...entities.comments,
                [parentId]: {
                    ...state[parentId],
                    children: {
                        ...result,
                        data: [
                            ...state[parentId].children.data,
                            ...result.data
                        ]
                    }
                }
            }            
        }
    }
})
const allIds = (state=[], action) => {
    switch (action.type) {
        case actionTypes.FETCH_COMMENTS.SUCCESS:
            return [
                ...state,
                ...action.payload.data.result.data
            ]
        case commentFormActionTypes.POST_COMMENT.SUCCESS:
            // result first to keep the comment data in a descending order.
            return [
                action.payload.data.result,
                ...state,
            ]
        case actionTypes.DELETE_COMMENT.SUCCESS:
            const newState = [...state]
            
            const idx = newState.indexOf(action.payload.commentId)
            // when there is matching
            if (idx !== -1) {
                newState.splice(idx, 1)
            }
            return newState
        default:
            return state
    }
}
const lastPostedId = (state=null, action) => {
    switch (action.type) {
        case commentFormActionTypes.POST_COMMENT.SUCCESS:
        case commentFormActionTypes.POST_REPLY.SUCCESS:
            return action.payload.data.result
        default:
            return state
    }
}
const comments = combineReducers({
    endId,
    count,
    byId,
    allIds,
    lastPostedId
})

const isEditingComment = (state=null, action) => {
    switch (action.type) {
        case actionTypes.CREATE_COMMENT:
            return true
        case actionTypes.CANCEL_CREATE_COMMENT:
            return false
        default:
            return state
    }
}
const commentTo = (state=null, action) => {
    switch (action.type) {
        case actionTypes.CREATE_COMMENT:
            return action.payload || null
        case actionTypes.CANCEL_CREATE_COMMENT:
            return null
        default:
            return state
    }
}
const commentToDelete = (state=null, action) => {
    switch (action.type) {
        case actionTypes.PREPARE_DELETE_COMMENT:
            return action.payload
        case actionTypes.CANCEL_DELETE_COMMENT:
        case actionTypes.DELETE_COMMENT.SUCCESS:
            return null
        default:
            return state
    }
}
export { comments, isEditingComment, commentTo, commentToDelete }
