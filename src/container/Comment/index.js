import React, { useEffect } from 'react'
import Comment from '../../component/Comment'
import Modal from '../../component/Modal'
import PasswordForm from '../../component/PasswordForm'
import { connect } from 'react-redux'
import { fetchCommentsAction, createCommentAction, deleteCommentAction, prepareDeleteComment, cancelDeleteComment } from './action'
import map from 'lodash/map'
import each from 'lodash/each'

const CommentContainer = props => {
    useEffect(() => {
        props.loadComment()
    }, [])
    return (
        <>
            <Comment
                data={props.data}
                lastPostedId={props.lastPostedId}
                replyHandler={props.replyHandler}
                onDelete={props.prepareToDelete}
            />
            <Modal show={props.isDeletingComment} onClick={props.cancelDeleteComment} >
                <PasswordForm
                    initialize={!props.isDeletingComment}
                    onClose={props.cancelDeleteComment}
                    onSubmit={props.submitHandler}
                />
            </Modal>
        </>
    )
}

const mapStateToProps = state => {
    const data = map(state.comments.allIds, parentId => {
        const parent = {...state.comments.byId[parentId]}
        parent.children = map(parent.children.data, childId => {
            return {...state.comments.byId[childId]}
        })
        return parent
    })
    return {
        data,
        lastPostedId: state.comments.lastPostedId,
        isDeletingComment: state.commentToDelete !== null
    }
}
const mapDispatchToProps = dispatch => {
    return {
        loadComment: () => {
            dispatch(fetchCommentsAction.request())
        },
        submitHandler: e => {
            const formData = {}
            each(
                e.currentTarget.querySelectorAll('input,textarea'),
                el => {
                    if (el.type !== 'submit') formData[el.name] = el.value
                }
            )
            const body = JSON.stringify(formData)
            dispatch(deleteCommentAction.request({body}))
        },
        cancelDeleteComment: e => {
            dispatch(cancelDeleteComment())
        },
        prepareToDelete: commentId => {
            dispatch(prepareDeleteComment(commentId))
        },
        replyHandler: commentId => {
            dispatch(createCommentAction(commentId))
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CommentContainer)
