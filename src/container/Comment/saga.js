import { call, takeEvery, select } from 'redux-saga/effects'

import { actionTypes, fetchCommentsAction, deleteCommentAction } from './action'
import { fetchComments, deleteComment } from '../../api'
import { apiSaga } from '../../util/saga'

function* fetchCommentsSaga() {
    yield call(
        apiSaga,
        fetchComments,
        fetchCommentsAction,
    )
}

function* deleteCommentSaga(action) {
    const commentId = yield select(state => state.commentToDelete)
    const body      = action.payload.body
    yield call(
        apiSaga,
        deleteComment,
        deleteCommentAction,
        {
            apiArgs: [commentId, body],
            actionPayload: { commentId }
        }
    )
}

function* commentSaga() {
    yield takeEvery(actionTypes.FETCH_COMMENTS.REQUEST, fetchCommentsSaga)
    yield takeEvery(actionTypes.DELETE_COMMENT.REQUEST, deleteCommentSaga)
}
export default commentSaga
