import { makeAsyncActions, makeAsyncActionCreator, makeActionCreator } from '../../util/action'

const PREFIX = '/COMMENT'

export const actionTypes = {
    FETCH_COMMENTS: makeAsyncActions(`${PREFIX}/FETCH_COMMENTS`),
    FETCH_REPLIES: makeAsyncActions(`${PREFIX}/FETCH_REPLIES`),
    DELETE_COMMENT: makeAsyncActions(`${PREFIX}/DELETE_COMMENT`),
    PREPARE_DELETE_COMMENT: `${PREFIX}/PREPARE_DELETE_COMMENT`,
    CANCEL_DELETE_COMMENT: `${PREFIX}/CANCEL_DELETE_COMMENT`,
    CREATE_COMMENT: `${PREFIX}/CREATE_COMMENT`,
    CANCEL_CREATE_COMMENT: `${PREFIX}/CANCEL_CREATE_COMMENT`
}
export const fetchCommentsAction = makeAsyncActionCreator(actionTypes.FETCH_COMMENTS)
export const fetchRepliesAction = makeAsyncActionCreator(actionTypes.FETCH_REPLIES)
export const prepareDeleteComment = makeActionCreator(actionTypes.PREPARE_DELETE_COMMENT)
export const cancelDeleteComment = makeActionCreator(actionTypes.CANCEL_DELETE_COMMENT)
export const deleteCommentAction = makeAsyncActionCreator(actionTypes.DELETE_COMMENT)
export const createCommentAction = makeActionCreator(actionTypes.CREATE_COMMENT)
export const cancelCreateCommentAction = makeActionCreator(actionTypes.CANCEL_CREATE_COMMENT)