import { actionTypes } from './action'

const weddingInfo = (state={}, action) => {
    switch (action.type) {
        case actionTypes.FETCH_WEDDING_DATA.SUCCESS:
            return action.payload.data
        default:
            return state
    }
}
export { weddingInfo }