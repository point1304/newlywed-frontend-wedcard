import React, { useEffect, useMemo } from 'react'
import { connect } from 'react-redux'
import Greetings from '../../component/Greetings'
import Gallery from '../../component/Gallery'
import Location from '../../component/Location'
import NmapLink from '../../component/NmapLink'
import AccessInfo from '../../component/AccessInfo'
import KakaoLocationLink from '../../component/KakaoLocationLink'
import WeddingContacts from '../../component/WeddingContacts'
import SectionSeparator from '../../component/SectionSeparator'
import sectionSeparatorImage from './sectionSeparator.png'
import CommentForm from '../../container/CommentForm'
import Comment from '../../container/Comment'
import CreateCommentButton from '../../container/CreateCommentButton'
import styles from './index.module.scss'
import { fetchWeddingDataAction } from './action.js'
import { cancelCreateCommentAction } from '../../container/Comment/action'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'
import camelize from 'camelize'
import { getRandomInt } from '../../util'
import classnames from 'classnames/bind'
const cx = classnames.bind(styles)

const MainPage = props => {
    useEffect(() => {
        if (!isEmpty(props.info)) return
        props.loadWeddingData()
    }, [])
    return (
        <>
            <div className={styles.page} onClick={props.clickPageContent}>
                {
                    !isEmpty(props.info) ?
                    <MainPageContents {...props} /> :
                    <div>Loading...</div>
                }
            </div>
            <CommentForm />
        </>
    )
}
const MainPageContents = (props) => {
    useEffect(() => {
        window.objectFitPolyfill && window.objectFitPolyfill()
        const { hash } = window.location
        if (hash) {
            const node = document.querySelector(hash)
            if (node) setTimeout(() => {node.scrollIntoView({behavior: 'smooth'})}, 600)
        }
    }, [])
    const mainPhoto = useMemo(
        () => props.info.mainPhoto ?
                props.info.mainPhoto.url :
                props.info.galleryPhotos[getRandomInt(0, props.info.galleryPhotos.length)],
        [props.info.galleryPhotos]
    )
    return (
        <>
            <section id='greetings' className={cx('greetings', '_')} >
                <Greetings
                    weddingOf={
                        `${props.info.contacts.groom.name} # ` +
                        `${props.info.contacts.bride.name}`
                    }
                    title={props.info.title}
                    message={props.info.message}
                    picURL={mainPhoto.url}
                    time={props.info.time}
                    loc={props.info.loc.detail}
                />
            </section>
            <div className={styles.sep}>
                <SectionSeparator
                    imageUrl={sectionSeparatorImage}
                    height={160}
                    width={600}
                    title={'CONTACTS'}
                />
            </div>
            <section id='contact' className={cx('contact', '_')}>
                <WeddingContacts
                    {...props.info.contacts}
                />
            </section>
            <section id='gallery' className={cx('gallery', '_')}>
                <Gallery col={3} srcs={map(props.info.galleryPhotos, obj => obj.url)} />
            </section>
            <section id='loc' className={cx('loc', '_')}>
                <p className={styles.title}>오시는 길</p>
                <div style={{height: '200px', borderTop: 'grey 1px solid'}}>
                    <Location {...props.info.loc}/>
                </div>
                <NmapLink {...props.info.loc}>
                    <div
                        style={{
                            height: '2.3em',
                            backgroundColor: 'grey',
                            color: 'white',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            fontSize: '1em'
                        }}
                    >
                        네이버맵 바로가기
                    </div>
                </NmapLink>
                <div style={{margin: '2em 0', padding: '0 1em'}}>
                    <AccessInfo {...props.info.loc.access} />
                </div>
            </section>
            <section id='comments' className={styles._}>
                <p className={styles.title}>축하메시지</p>
                <CreateCommentButton />
                <Comment />
            </section>
            <section className={styles.share} id='share'>
                공유하기 :
                <KakaoLocationLink
                    containerId='sharebykakao'
                    api_key='6ad01de0a9cd6e11fa8b555ec662a6a9'
                    address={props.info.loc.address}
                    addressTitle={props.info.loc.detail}
                    title={`${props.info.contacts.groom.name} & ${props.info.contacts.bride.name}의 결혼식에 초대합니다`}
                    description='#결혼 #wedding'
                    imageUrl={mainPhoto.url}
                    url={document.location.origin}
                    buttonTitle='청첩장 보기'
                >
                    카카오톡
                </KakaoLocationLink>
                |
                <a href={`sms://&body=${encodeURIComponent(document.location.origin)}`}>
                    문자메시지
                </a>
            </section>
        </>
    )
}
const mapStateToProps = state => {
    return {
        info: state.weddingInfo,
        isEditingComment: state.isEditingComment,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        loadWeddingData: () => {
            if (window.__REDUX_STATE__) {
                const wedProfileFromSsr = camelize(window.__REDUX_STATE__)
                delete window.__REDUX_STATE__
                dispatch(fetchWeddingDataAction.success({data: wedProfileFromSsr}))
            }
            else {
                dispatch(fetchWeddingDataAction.request())
            }
        },
        clickPageContent: () => {
            dispatch(cancelCreateCommentAction())
        }
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MainPage)
