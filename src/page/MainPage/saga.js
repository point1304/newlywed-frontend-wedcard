import { call, takeEvery } from 'redux-saga/effects'
import { apiSaga } from '../../util/saga'
import { actionTypes, fetchWeddingDataAction } from './action'
import { fetchWeddingData } from '../../api'

function* fetchWeddingDataSaga() {
    yield call(apiSaga,
                fetchWeddingData,
                fetchWeddingDataAction)
}
function* mainPageSaga() {
    yield takeEvery(actionTypes.FETCH_WEDDING_DATA.REQUEST, fetchWeddingDataSaga)
}
export default mainPageSaga
