import { makeAsyncActions, makeAsyncActionCreator, makeActionCreator } from '../../util/action'

const PREFIX = '/MAINPAGE'

export const actionTypes = {
    FETCH_WEDDING_DATA: makeAsyncActions(`${PREFIX}/FETCH_WEDDING_DATA`),
    FETCH_SOMETHING: makeAsyncActions(`${PREFIX}/FETCH_SOMETHING`),
    POST_SOMETHING: makeAsyncActions(`${PREFIX}/POST_SOMETHING`),
    CLICK_PAGE_CONTENT: `${PREFIX}/CLICK_PAGE_CONTENT`
}
export const fetchWeddingDataAction = makeAsyncActionCreator(actionTypes.FETCH_WEDDING_DATA)
export const clickPageContentAction = makeActionCreator(actionTypes.CLICK_PAGE_CONTENT)