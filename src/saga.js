import { all } from 'redux-saga/effects'
import mainPageSaga from './page/MainPage/saga'
import commentSaga from './container/Comment/saga'
import commentFormSaga from './container/CommentForm/saga'

export default function* rootSaga() {
    yield all([mainPageSaga(), commentSaga(), commentFormSaga()])
}
