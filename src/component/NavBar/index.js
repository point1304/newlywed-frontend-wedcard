import React from 'react'
import { NavHashLink as NavLink } from 'react-router-hash-link'
import map from 'lodash/map'
import styles from './index.module.scss'

const NavBar = ({links}) => (
    <div className={styles.links}> 
        {
            map(links, link => (
                <NavLink
                    exact={link.to === '/'}
                    key={link.to}
                    className={styles.link}
                    activeClassName={styles.active}
                    to={link.to}
                    smooth
                >
                    {link.name}
                    {/*<span className={styles._} />*/}
                </NavLink>
            ))
        }
    </div>
)
export default NavBar
