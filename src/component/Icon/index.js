import React from 'react'
import styles from './index.module.scss'

const size = ['xs', 'sm', '', 'lg', '2x', '3x', '4x', '5x', '6x', '7x', '8x', '9x', '10x']

const Base = props => {
    const iconSize = (props.size || props.size === 0) ? size[props.size] : ''
    const className = props.defaultCssOff ?
                        `${props.className}` :
                        props.className ?
                        `${props.className} ${styles._}`
                        : styles._
    return (
        <a href={props.href} className={className} style={props.style}>
            <i className={`fas fa-${props.name}${iconSize && ' fa-' + iconSize}`}></i>
        </a>
    )
}
export const PhoneIcon = props => (
    <Base name='phone' {...props} />
)
export const KakaoIcon = props => (
    <Base name='comments' {...props} />
)
export const SMSIcon = props => (
    <Base
        name='sms'
        {...{...props, size: !props.size && props.size !== 0 ? 3: props.size}}
    />
)
export const ChatBubbleIcon = props => (
    <Base
        name='comment'
        {...props}
    />
)