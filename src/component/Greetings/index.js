import React from 'react'
import Image from '../Image'
import styles from './index.module.scss'
import reduce from 'lodash/reduce'

const toFormattedDateOfSeoulTimezone = (ts) => {
    // ts in milliseconds
    ts = ts * 1000
    let pad = num => {
        let norm = Math.floor(Math.abs(num))
        return (norm < 10 ? '0' : '') + norm
    }
    let days = ['일','월','화','수','목','금','토'],
        offset = new Date().getTimezoneOffset(),
        time   = new Date(ts + ((540 + offset) * 60 * 1000)),
        year   = time.getFullYear(),
        month  = pad(time.getMonth() + 1),
        date   = pad(time.getDate()),
        hours  = time.getHours() === 12 ? 12 : time.getHours() % 12,
        min    = pad(time.getMinutes()),
        ampm   = time.getHours() < 12 ? '오전' : '오후',
        day    = time.getDay()
    return `${year}. ${month}. ${date} ${days[day]}요일\u00A0\u00A0\u00A0${ampm} ${hours}:${min}`
}
const Greetings = ({weddingOf, title, message, picURL, time, loc}) => (
    <>
        <div className={styles.wrap}>
            <span className={styles.mainMsg}>{title}</span>
            {/** msFlexNegative + height auto is for ie flexbox bugfix */}
            <Image src={picURL} style={{msFlexNegative: '0', height: 'auto'}} />
            <h2 style={{marginTop: '3em', fontSize: '1em'}}>{weddingOf}</h2>
            <div className={styles.info}>
                <p style={{fontWeight: 800}}>{toFormattedDateOfSeoulTimezone(time)}</p>
                <p>{loc}</p>
            </div>
            <div className={styles.msg}>
                <p><span>INVITATION</span></p>
                <p>
                    {reduce(message.split(/\r?\n/),
                        (acc, cur) => (
                            <>{acc}<br/>{cur}</>))
                    }
                </p>
            </div>
        </div>
    </>
)
export default Greetings
