import React, { useState } from 'react'
import styles from './index.module.scss'
import each from 'lodash/each'
import map from 'lodash/map'
import classNames from 'classnames/bind'
const cx = classNames.bind(styles)

const WrapperForFocusEffect = props => {
    const [focus, setFocus] = useState(false)

    const handleFocus = e => {
        e.stopPropagation()
        setFocus(true)
        props.onFocus && props.onFocus(e)
    }
    const handleBlur = e => {
        e.stopPropagation()
        setFocus(false)
        props.onBlur && props.onBlur(e)
    }
    const handleChange = e => {
        props.onChange && props.onChange(e)
    }
    return (
        <div className={cx('input', {focus: focus, invalid: props.isInvalid})}>
            {
                props.children({
                    ...props,
                    children: undefined,
                    onFocus: handleFocus,
                    onBlur: handleBlur,
                    onChange: handleChange,
                })
            }
        </div>
    )
}
export const Input = props => {
    return (
        <WrapperForFocusEffect {...props}>
            {
                props => (
                    <label>
                        <div className={styles.inputTitle}>{props.title}</div>
                        <input
                            type={props.type}
                            name={props.name}
                            onFocus={props.onFocus}
                            onBlur={props.onBlur}
                            onChange={props.onChange}
                            required={props.required}
                            maxLength={props.maxLength}
                            minLength={props.minLength}
                        />
                    </label>
                )
            }
        </WrapperForFocusEffect>
    )
}

export const TextAreaAndSubmit = props => {
    return (
        <WrapperForFocusEffect {...props}>
            {
                props => (
                    <label>
                        <div className={styles.inputTitle}>{props.title}</div>
                        <textarea
                            name={props.name}
                            onFocus={props.onFocus}
                            onBlur={props.onBlur}
                            onChange={props.onChange}
                            required={props.required}
                            maxLength={props.maxLength}
                            minLength={props.minLength}
                        />
                        <input className={styles.submit} type="submit" value="게시" />
                    </label>
                )
            }
        </WrapperForFocusEffect>
    )
}
export default function(props) {
    const [formData, setFormData] = useState({})

    const validate = () => {
        let isValid = true

        const newFormData = {...formData}
        each(newFormData, (v, k) => {
            const ptn = props.schema[k].pattern
            const judge = ptn ? ptn.test(v.value) : true
            v.isInvalid = !judge
            if (judge === false) isValid = false
        })
        setFormData(newFormData)
        return isValid
    }
    const handleSubmit = e => {
        e.stopPropagation()
        e.preventDefault()
        validate() && props.onSubmit && props.onSubmit(e)
    }
    const handleChange = e => {
        const newFormData = {
            ...formData,
            [e.currentTarget.name]: {
                ...formData[e.currentTarget.name],
                value: e.currentTarget.value
            }
        }
        setFormData(newFormData)
    }
    return (
        <div className={styles._}>
            <form onSubmit={handleSubmit}>
                {
                    map(props.order, itemName => {
                        const item = props.schema[itemName]
                        const Component = item.component
                        return (
                            <Component
                                {...item}
                                key={itemName}
                                onChange={
                                    item.onChange ?
                                    e => {item.onChange(e); handleChange(e)} :
                                    handleChange
                                }
                                name={itemName}
                                isInvalid={formData[itemName] && formData[itemName].isInvalid}
                            />
                        )
                    })
                }
            </form>
        </div>
    )
}
