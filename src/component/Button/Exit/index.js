import React from 'react'
import styles from './index.module.scss'

export default function({onClick}) {
    return (
        <div className={styles.wrap} onClick={onClick}>
            <div className={styles._}>
                <span></span>
                <span></span>
            </div>
        </div>
    )
}