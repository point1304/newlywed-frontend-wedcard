import React from 'react'
import styles from './index.module.scss'
import map from 'lodash/map'

const LINEBREAK_PTN = /\r?\n/
const BOLD_PTN = /\[\[(.*)\]\]/
const SPLIT_PTN = /(\r?\n|\[\[.*\]\])/
const Case = props => (
    props.text &&
    <div className={styles.case}>
        <div className={styles.title}>{props.title}</div>
        <div className={styles.content}>
            {
                map(
                    props.text.split(SPLIT_PTN),
                    (el, i) => {
                        if (LINEBREAK_PTN.test(el)) {
                            return <br key={i}/>
                        }
                        else {
                            const match = el.match(BOLD_PTN)
                            if (match) {
                                return <span key={i} className={styles.bold}>{match[1]}</span>
                            }
                            else {
                                return el
                            }
                        }
                    }
                )
            }
        </div>
    </div>
)

export default function(props) {
    return (
        <div className={styles._}>
            <Case text={props.byCar} title="자가용 이용시" />
            <Case text={props.byBus} title="버스 이용시" />
            <Case text={props.byMetro} title="지하철 이용시" />
        </div>
    )
}
