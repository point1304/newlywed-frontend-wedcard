import React, { useEffect } from 'react'
import ExitButton from '../Button/Exit'
import './lastPosted.scss'
import styles from './index.module.scss'

import { timeSince } from '../../util'
import map from 'lodash/map'
import reduce from 'lodash/reduce'

const Comment = props => {
    return (
        <div className={styles.wrap} id={props.lastPosted ? 'lastPosted' : null}>
            <div className={styles.u}>
                <span className={styles.name}>{props.name}</span>
                <span className={styles.ip}>{props.ip}</span>
            </div>
            <div className={styles.m}>
                <div className={styles.text}>{reduce(props.text.split(/\r?\n/), (acc, cur) => <>{acc}<br/>{cur}</>)}</div>
                <div className={styles.del}>
                    <ExitButton onClick={props.deleteHandler} />
                </div>
            </div>
            <div className={styles.l}>
                <span>{timeSince(props.ts * 1000)}</span>
                <span onClick={props.replyHandler} style={{cursor: 'pointer'}}>답글달기</span>
            </div>
        </div>
    )
}

const CommentSet = props => {
    return (
        <>
            <Comment
                {...props}
                lastPosted={props.lastPostedId === props.id}
                deleteHandler={() => {props.deleteHandler(props.id)}}
                replyHandler={e => {e.stopPropagation(); props.replyHandler(props.id)}}
            />
            <div className={styles.re}>
                {
                    map(props.children, child => (
                        <Comment
                            key={child.id}
                            {...child}
                            lastPosted={child.id === props.lastPostedId}
                            deleteHandler={() => {props.deleteHandler(child.id)}}
                            replyHandler={e => {e.stopPropagation(); props.replyHandler(props.id)}}
                        />
                    ))
                }
            </div>
        </>
    )
}

export default function(props) {
    useEffect(() => {
        const node = document.querySelector('#lastPosted')
        if (node) node.scrollIntoView({behavior: 'smooth', block: 'center'})
    }, [props.lastPostedId])
    return (
        <div className={styles._}>
            {
                map(props.data, el => (
                    <CommentSet
                        key={el.id}
                        {...el}
                        lastPostedId={props.lastPostedId}
                        deleteHandler={props.onDelete}
                        replyHandler={props.replyHandler}
                    />
                ))
            }
        </div>
    )
}