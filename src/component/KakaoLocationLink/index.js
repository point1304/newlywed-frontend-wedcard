import React from 'react'
import styles from './index.module.scss'

class KakaoLocationLink extends React.PureComponent {
    constructor(props) {
        super(props)
        this.state = {SDKisLoaded: false}
    }
    makeKakaoLink = () => {
        window.Kakao.init(this.props.api_key)
        window.Kakao.Link.createDefaultButton({
            container: `#${this.props.containerId}`,
            objectType: 'location',
            address: this.props.address,
            addressTitle: this.props.addressTitle,
            content: {
                title: this.props.title,
                description: this.props.description,
                imageUrl: this.props.imageUrl,
                link: {
                    mobileWebUrl: this.props.url,
                    webUrl: this.props.url
                }
            },
            buttons: [{
                title: this.props.buttonTitle,
                link: {
                    mobileWebUrl: this.props.url,
                    webUrl: this.props.url
                }
            }]
        })
    }
    componentDidMount() {
        const SDKisLoaded = document.getElementById('Kakao')
        if (!SDKisLoaded) {
            const script = document.createElement('script')
            script.async = true
            script.src = '//developers.kakao.com/sdk/js/kakao.min.js'
            script.id  = 'Kakao'
            document.body.appendChild(script)
            script.onload = () => {
                this.makeKakaoLink()
                this.setState({...this.state, SDKisLoaded: true})}
        } else {
            this.makeKakaoLink()
            this.setState({...this.state, SDKisLoaded: true})
        }
    }
    render() {
        return (
            <div className={styles._} id={this.props.containerId}>
                {this.props.children}
            </div>
        )
    }
}
export default KakaoLocationLink
