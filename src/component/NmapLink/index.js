import React from 'react'

export default function (props) {
    const actionUri = encodeURI(`nmap://search?query=${props.name}`)
    const fallback = `https://map.naver.com/v5/search/${props.name}`
    let timer, heartbeat

    const clearTimers = () => {
        clearTimeout(timer)
        clearInterval(heartbeat)
    }

    const intervalHeartbeat = () => {
        if (document.webkitHidden || document.hidden) {
            clearTimers()
        }
    }

    const tryWebkitApproach = () => {
        document.location = actionUri
        timer = setTimeout(() => {
            document.location = fallback
        }, 3000)
    }

    const onClick = e => {
        e.stopPropagation()
        heartbeat = setInterval(intervalHeartbeat, 200)
        tryWebkitApproach()
        /*
        if (navigator.userAgent.match(/iPhone|iPad|iPod/)) {
            let clickedAt = +new Date()
            console.log(props)
            //window.location.href = `nmap://route/car?dlat=${props.lat}&dlng=${props.lng}&dname=${props.name}`
            window.location.href = encodeURI(`nmap://search?query=${props.name}`)

            setTimeout(() => {
                if (+new Date() - clickedAt < 2000) {
                    window.location.href = `http://app.map.naver.com/launchApp/?version=11&elat=${props.lat}&elng=${props.lng}&etitle=${props.name}`
                }
            })
        }
        else {
            window.location.href = `https://map.naver.com/v5/search/${props.name}`
        }*/
    }
    return (
        <div style={{width: '100%', cursor: 'pointer'}} onClick={onClick}>
            {props.children}
        </div>
    )
}
