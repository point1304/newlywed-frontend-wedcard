import React from 'react'
import CSSTransitionGroup from 'react-transition-group/CSSTransition'
import Image from '../Image'
import ExitButton from '../Button/Exit'
import Carousel from '../Carousel'
import styles from './index.module.scss'

import map from 'lodash/map'
import ceil from 'lodash/ceil'
import classNames from 'classnames/bind'
const cx = classNames.bind(styles)

const lToClose = 200
// const lToSlide = 130 
const dragToFadeOutRate = lToClose + 100

class Gallery extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            zoom: false,
            selectedPicIdx: null,
            slideNav: false,
        }

        this.row = ceil(this.props.srcs.length / this.props.col)
        this.width = `calc(100% / ${this.props.col} - ${(this.props.col - 1) / this.props.col}vw)`

        this.initY = null
        this.curY = null
        this.initX = null
        this.curX = null
        this.vMove = null

        this.sliderRef = React.createRef()
        this.dummyRef = React.createRef()
        this.carouselRef = React.createRef()
    }
    onTouchStart = e => {
        e.stopPropagation()
        this.initX = e.touches[0].clientX
        this.initY = e.touches[0].clientY
    }
    onTouchMove = e => {
        e.stopPropagation()
        if (!this.state.zoom) return
        this.curX = e.touches[0].clientX
        this.curY = e.touches[0].clientY
        let offsetX = this.curX - this.initX
        let offsetY = this.curY - this.initY
        if (this.vMove === null) {
            // determine vMove and calibrate starting point of touch
            if (Math.sqrt(offsetX ** 2 + offsetY ** 2) < 5) return
            if (Math.abs(offsetX) < Math.abs(offsetY)) this.vMove = true
            else this.vMove = false
            this.initX = this.curX
            this.initY = this.curY
            offsetX = 0
            offsetY = 0
        }
        if (this.vMove === true) {
            this.sliderAnimation.setOpacity(e, 1 - Math.abs(offsetY / dragToFadeOutRate))
        }
        // else if (this.vMove === false) {
        //    this.sliderAnimation.slideLateral(e, offsetX)
        // }
    }
    onTouchEnd = e => {
        e.stopPropagation()
        if (!this.state.zoom) {
            this.vMove = null
            return
        }
        const offsetX = this.curX - this.initX
        const offsetY = this.curY - this.initY

        if (this.vMove === true) {
            if (Math.abs(offsetY) >= lToClose) this.sliderAnimation.fadeout(e)
            else this.sliderAnimation.fadein(e)
        }
        //else if (this.vMove === false) {
        //    if (Math.abs(offsetX) >= lToSlide) {
        //        if (offsetX > 0) this.sliderAnimation.slidePrev(e)
        //        else this.sliderAnimation.slideNext(e)
        //    }
        //    else this.sliderAnimation.returnLateral(e)
        //}
        this.vMove = null
    }
    onImageClick = idx => {
        this.sliderAnimation.fadein({currentTarget: this.sliderRef.current})
        this.setState({...this.state, selectedPicIdx: idx, slideNav: true})
    }
    onSlideImgClick = e => {
        e.stopPropagation()
        this.setState({...this.state, slideNav: !this.state.slideNav})
    }
    onSlideNextBtnClick = e => {
        e.stopPropagation()
        this.sliderAnimation.initLeftToZero()
        this.sliderAnimation.slideNext()
    }
    onSlidePrevBtnClick = e => {
        e.stopPropagation()
        this.sliderAnimation.initLeftToZero()
        this.sliderAnimation.slidePrev()
    }
    createAnimation = () => {
        let hasNextTick = false
        let rAFId
        let initOpacity
        let curOpacity
        let initLeft
        let nextImgPosX
        let prevImgPosX
        const loop = (evt, render) => {
            if (hasNextTick !== false) return
            let lastFrame
            let initFrame
            const el = evt !== undefined ? evt.currentTarget : null
            const step = now => {
                //this.dummyRef.current.textContent = 'a' 
                if (!initFrame) initFrame = lastFrame = now
                if (now - lastFrame < 1000) {
                    hasNextTick = render(now - initFrame, el)
                    if (hasNextTick !== false) {
                        rAFId = requestAnimationFrame(step)
                    }
                }
                lastFrame = now
            }
            rAFId = requestAnimationFrame(step)
            /*
            const step = now => {
                if (isRunning !== false) {
                    // write in dummyRef to handle iOS Safari rAF glitch.
                    this.dummyRef.current.textContent = 'a'
                    if (!initFrame) initFrame = lastFrame = now
                    if (now - lastFrame < 1000) {
                        hasNextTick = render(now - initFrame, el)
                        rAFId = requestAnimationFrame(step)
                        if (hasNextTick === false) isRunning = false
                    }
                    lastFrame = now
                }
            }
            requestAnimationFrame(step)
            */
        }
        const tick = (evt, render) => {
            if (hasNextTick !== false) return
            const el = evt.currentTarget
            const step = () => {
                //this.dummyRef.current.textContent = 'a'
                render(el)
                // `isRunnign = false` comes with glitches when
                // fadeout or fadein action need to be triggered
                // right after the tick.
            }
            rAFId = requestAnimationFrame(step)
        }
        const ret = {
            fadein: e => {loop(e, (elapsed, el) => {
                if (initOpacity === undefined) initOpacity = 0
                curOpacity = initOpacity + elapsed / 300
                if (curOpacity >= 1) {
                    el.style.opacity = ''
                    initOpacity = undefined
                    curOpacity = undefined
                    return false
                }
                else {
                    el.style.opacity = curOpacity
                    if (!this.state.zoom) {
                        this.setState(prev => ({...prev, zoom: true}))
                        window.objectFitPolyfill && window.objectFitPolyfill()
                    }
                }
                // else el.style.backgroundColor = `rgba(0,0,0,${curOpacity})`
            })},
            fadeout: e => {loop(e, (elapsed, el) => {
                el = this.sliderRef.current
                if (initOpacity === undefined) initOpacity = 1
                curOpacity = initOpacity - elapsed / 300
                if (curOpacity <= 0) {
                    el.style.opacity = ''
                    initOpacity = undefined
                    curOpacity = undefined
                    this.setState({...this.state, zoom: false})
                    return false
                }
                else el.style.opacity = curOpacity
                // else el.style.backgroundColor = `rgba(0,0,0,${curOpacity})`
            })},
            setOpacity: (e, opacity) => {
                tick(e, el => {
                    initOpacity = opacity
                    el.style.opacity = opacity
                    // el.style.backgroundColor = `rgba(0,0,0,${opacity})`
                })
            },
            stop: () => {
                cancelAnimationFrame(rAFId)
                hasNextTick = false
            }
        }
        return ret
    }
    sliderAnimation = this.createAnimation()
    componentDidMount() {
        this.setState({...this.state})
    }
    render() {
        const len = this.props.srcs.length
        const selectedIdx = (this.state.selectedPicIdx % len + len) % len
        const that = this
        return (
            <div
                className={styles._}
                style={{paddingTop: `calc((100% - ${this.props.col - 1}vw) * ${this.row / this.props.col} + ${this.row - 1}vw)`}}
            >
                <div className={styles.wrap}>
                    {map(this.props.srcs, (src, i) => (
                        <div
                            key={i}
                            className={
                                cx('outer')
                            }
                            style={{
                                width: this.width,
                                paddingTop: this.width
                            }}
                            onClick={() => {this.onImageClick(i)}}
                        >
                            <div className={styles.inner}>
                                <Image src={src} cover />
                            </div>
                        </div>
                    ))}
                </div>
                <div
                    ref={this.sliderRef}
                    className={cx('slider', {zoom: this.state.zoom})}
                    onTouchStart={this.onTouchStart}
                    onTouchMove={this.onTouchMove}
                    onTouchEnd={this.onTouchEnd}
                >
                    {
                        this.state.zoom ?
                        <Carousel startIndex={this.state.selectedPicIdx} onChange={function() {that.setState({...that.state, selectedPicIdx: this.currentSlide})}} loop propagate ref={this.carouselRef}>
                            {
                                this.props.srcs.map(src => {
                                    return (
                                        <img
                                            key={src}
                                            className={styles.sliderImg}
                                            src={src}
                                            onClick={this.onSlideImgClick}
                                            data-object-fit={'contain'}
                                        />
                                    )
                                })
                            }
                        </Carousel> : null
                    }
                    <CSSTransitionGroup
                        in={this.state.slideNav}
                        unmountOnExit
                        classNames={{
                            enter: styles.uiEnter,
                            enterActive: styles.uiEnterActive,
                            exit: styles.uiExit,
                            exitActive: styles.uiExitActive,
                        }}
                        timeout={300}
                    >
                        <div className={styles.ui}>
                            <div className={styles.slideInfo}>
                                <span style={{color: 'white'}}>{`${selectedIdx + 1} / ${len}`}</span>
                                <div className={styles.exit}>
                                    <ExitButton onClick={this.sliderAnimation.fadeout}/>
                                </div>
                            </div>
                            <button
                                className={cx('btn', 'next')}
                                onClick={() => {this.carouselRef.current.next()}}
                            />
                            <button className={cx('btn', 'prev')}
                                onClick={() => {this.carouselRef.current.prev()}}
                            />
                        </div>
                    </CSSTransitionGroup>
                    <div ref={this.dummyRef}></div>
                </div>
            </div>
        )
    }
}

export default Gallery
