import React from 'react'
import styles from './index.module.scss'
import classNames from 'classnames/bind'
const cx = classNames.bind(styles)

class ReactSiema extends React.Component {
    constructor(props) {
        super(props)
        this.config = {
            duration: 200,
            easing: 'ease-out',
            perPage: 1,
            startIndex: 0,
            draggable: true,
            multipleDrag: true,
            threshold: 20,
            loop: false,
            rtl: false,
            onInit: () => {},
            onChange: () => {},
            ...props
        }
        this.selector = React.createRef()
        this.sliderFrame = React.createRef()
    }
    componentDidMount() {
        this.init()
    }
    componentWillUnmount() {
        this.destroy()
    }
    init() {
        // create global references
        this.selectorWidth = this.selector.current.offsetWidth
        this.currentSlide = this.config.loop ?
            this.config.startIndex % React.Children.count(this.props.children) :
            Math.max(0, Math.min(this.config.startIndex, React.Children.count(this.props.children) - this.config.perPage))
        this.transformProperty = ReactSiema.webkitOrNot()

        this.attachEvents()
        // hide everything out of selector's boundaries
        this.selector.current.style.overflow = 'hidden'
        // rtl or ltr
        this.selector.current.style.direction = this.config.rtl ? 'rtl' : 'ltr'
        // slide to a currentSlide
        this.slideToCurrent()

        this.config.onInit.call(this)
    }
    /**
     * moves sliders frame to position of currently active slide
     */
    slideToCurrent(enableTransition) {
        const currentSlide = this.config.loop ?
                                this.currentSlide + this.config.perPage :
                                this.currentSlide
        const offset = (this.config.rtl ? 1 : -1) * currentSlide * (this.selectorWidth / this.config.perPage)

        if (enableTransition) {
            // this one is tricky, I know but this is a perfect explanation:
            // https://youtu.be/cCOL7MC4P10
            requestAnimationFrame(() => {
                requestAnimationFrame(() => {
                    this.enableTransition()
                    this.sliderFrame.current.style[this.transformProperty] = `translate3d(${offset}px,0,0)`
                })
            })
        }
        else {
            this.sliderFrame.current.style[this.transformProperty] = `translate3d(${offset}px,0,0)`
        }
    }
    /**
     * go to previous slide.
     * @param {number} [howManySlides=1] - How many items to slide backward.
     * @param {function} callback - Optional callback function.
     */
    prev(howManySlides = 1, callback) {
        // early return when there is nothing to slide
        if (React.Children.count(this.props.children) <= this.config.perPage) {
            return
        }

        const beforeChange = this.currentSlide

        if (this.config.loop) {
            const isNewIndexClone = this.currentSlide - howManySlides < 0
            if (isNewIndexClone) {
                this.disableTransition()

                const mirrorSlideIndex = this.currentSlide + React.Children.count(this.props.children)
                const mirrorSlideIndexOffset = this.config.perPage
                const moveTo = mirrorSlideIndex + mirrorSlideIndexOffset
                const offset = (this.config.rtl ? 1 : -1) * moveTo * (this.selectorWidth / this.config.perPage)
                const dragDistance = this.config.draggable ? this.drag.endX - this.drag.startX : 0

                this.sliderFrame.current.style[this.transformProperty] = `translate3d(${offset + dragDistance}px,0,0)`
                this.currentSlide = mirrorSlideIndex - howManySlides
            }
            else {
                this.currentSlide = this.currentSlide - howManySlides
            }
        }
        else {
            this.currentSlide = Math.max(this.currentSlide - howManySlides, 0)
        }

        if (beforeChange !== this.currentSlide) {
            this.slideToCurrent(this.config.loop)
            this.config.onChange.call(this)
            if (callback) callback.call(this)
        }
    }
    /**
     * go to next slide.
     * @param {number} [howManySlides=1] - How many items to slide forward
     * @param {function} callback - Optional callback function
     */
    next(howManySlides = 1, callback) {
        // early return when there is nothing to slide
        if (React.Children.count(this.props.children) <= this.config.perPage) return

        const beforeChange = this.currentSlide

        if (this.config.loop) {
            const isNewIndexClone = this.currentSlide + howManySlides > React.Children.count(this.props.children) - this.config.perPage
            if (isNewIndexClone) {
                this.disableTransition()

                const mirrorSlideIndex = this.currentSlide - React.Children.count(this.props.children)
                const mirrorSlideIndexOffset = this.config.perPage
                const moveTo = mirrorSlideIndex + mirrorSlideIndexOffset
                const offset = (this.config.rtl ? 1 : -1) * moveTo * (this.selectorWidth / this.config.perPage)
                const dragDistance = this.config.draggable ? this.drag.endX - this.drag.startX : 0

                this.sliderFrame.current.style[this.transformProperty] = `translate3d(${offset + dragDistance}px,0,0)`
                this.currentSlide = mirrorSlideIndex + howManySlides
            }
            else {
                this.currentSlide = this.currentSlide + howManySlides
            }
        }
        else {
            this.currentSlide = Math.min(
                this.currentSlide + howManySlides,
                React.Children.count(this.props.children) - this.config.perPage
            )
        }
        if (beforeChange !== this.currentSlide) {
            this.slideToCurrent(this.config.loop)
            this.config.onChange.call(this)
            if (callback) {
                callback.call(this)
            }
        }
    }
    /**
     * Go to slide with particular index
     * @param {number} index - Item index to slide to
     * @param {function} callback - Optional callback function
     */
    goTo(index, callback) {
        if (React.Children.count(this.props.children) <= this.config.perPage) {
            return
        }
        const beforeChange = this.currentSlide
        this.currentSlide = this.config.loop ?
            index % React.Children.count(this.props.children) :
            Math.min(Math.max(index, 0), React.Children.count(this.props.children) - this.config.perPage)
        if (beforeChange !== this.currentSlide) {
            this.slideToCurrent()
            this.config.onChange.call(this)
            if (callback) {
                callback.call(this)
            }
        }
    }
    /**
     * Disable transition on sliderFrame
     */
    disableTransition() {
        const value = `all 0ms ${this.config.easing}`
        this.sliderFrame.current.style.webkitTransition = value
        this.sliderFrame.current.style.transition = value
    }
    /**
     * Enable transition on sliderFrame
     */
    enableTransition() {
        const value = `all ${this.config.duration}ms ${this.config.easing}`
        this.sliderFrame.current.style.webkitTransition = value
        this.sliderFrame.current.style.transition = value
    }
    /**
     * Recalculate drag /swipe event and reposition the frame of a slider
     */
    updateAfterDrag() {
        const movement = (this.config.rtl ? -1 : 1) * (this.drag.endX - this.drag.startX)
        const movementDistance = Math.abs(movement)
        const howManySliderToSlide = this.config.multipleDrag ? Math.ceil(movementDistance / (this.selectorWidth / this.config.perPage)) : 1

        const slideToNegativeClone = movement > 0 && this.currentSlide - howManySliderToSlide < 0
        const slideToPositiveClone = movement < 0 && this.currentSlide + howManySliderToSlide > React.Children.count(this.props.children) - this.config.perPage

        if (movement > 0 && movementDistance > this.config.threshold && React.Children.count(this.props.children) > this.config.perPage) {
            this.prev(howManySliderToSlide)
        }
        else if (movement < 0 && movementDistance > this.config.threshold && React.Children.count(this.props.children) > this.config.perPage) {
            this.next(howManySliderToSlide)
        }
        this.slideToCurrent(slideToNegativeClone || slideToPositiveClone)
    }

    /**
     * When window resizes, resize slider components as well
     */
    resizeHandler = e => {
        this.selectorWidth = this.selector.current.offsetWidth
        this.disableTransition()
        this.slideToCurrent()
    }

    /**
     * Clear drag after touchend and mouseup event
     */
    clearDrag() {
        this.drag = {
            startX: 0,
            endX: 0,
            startY: 0,
            letItGo: null,
            preventClick: this.drag.preventClick
        }
    }

    /**
     * touchstart event handler
     */
    touchstartHandler = e => {
        // Prevent dragging / swiping on inputs, selects and textareas
        const ignoreSiema = ['TEXTAREA', 'OPTION', 'INPUT', 'SELECT'].indexOf(e.target.nodeName) !== -1
        if (ignoreSiema) {
            return
        }

        !this.props.propagate && e.stopPropagation()
        this.pointerDown = true
        this.drag.startX = e.touches[0].pageX
        this.drag.startY = e.touches[0].pageY
    }

    /**
     * touchend event handler
     */
    touchendHandler = e => {
        !this.props.propagate && e.stopPropagation()
        this.pointerDown = false
        this.enableTransition()
        if (this.drag.endX) {
            this.updateAfterDrag()
        }
        this.clearDrag()
    }

    /**
     * touchmove event handler
     */
    touchmoveHandler = e => {
        !this.props.propagate && e.stopPropagation()

        if (this.drag.letItGo == null) {
            this.drag.letItGo = Math.abs(this.drag.startY - e.touches[0].pageY) < Math.abs(this.drag.startX - e.touches[0].pageX)
        }

        if (this.pointerDown && this.drag.letItGo) {
            e.preventDefault()
            this.drag.endX = e.touches[0].pageX

            const value = `all 0ms ${this.config.easing}`
            this.sliderFrame.current.style.webkitTransition = value
            this.sliderFrame.current.style.transition = value

            const currentSlide = this.config.loop ? this.currentSlide + this.config.perPage : this.currentSlide
            const currentOffset = currentSlide * (this.selectorWidth / this.config.perPage)
            const dragOffset = (this.drag.endX - this.drag.startX)
            const offset = this.config.rtl ? currentOffset + dragOffset : currentOffset - dragOffset
            this.sliderFrame.current.style[this.transformProperty] = `translate3d(${(this.config.rtl ? 1 : -1) * offset}px,0,0)`
        }
    }

    /**
     * mousedown event handler
     */
    mousedownHandler = e => {
        // Prevent dragging / swiping on inputs, selects and textareas
        const ignoreSiema = ['TEXTAREA', 'OPTION', 'INPUT', 'SELECT'].indexOf(e.target.nodeName) !== -1
        if (ignoreSiema) {
            return
        }

        e.preventDefault()
        !this.props.propagate && e.stopPropagation()
        this.pointerDown = true
        this.drag.startX = e.pageX
    }

    /**
     * mouseup event handler
     */
    mouseupHandler = e => {
        !this.props.propagate && e.stopPropagation()
        this.pointerDown = false
        this.selector.current.style.cursor = '-webkit-grab'
        this.enableTransition()
        if (this.drag.endX) {
            this.updateAfterDrag()
        }
        this.clearDrag()
    }

    /**
     * mousemove event handler
     */
    mousemoveHandler = e => {
        e.preventDefault()
        if (this.pointerDown) {
            // if dragged element is a link
            // mark preventClick prop as a true
            // to determine about browser redirection later on
            if (e.target.nodeName === 'A') {
                this.drag.preventClick = true
            }

            this.drag.endX = e.pageX
            this.selector.current.style.cursor = '-webkit-grabbing'

            const value = `all 0ms ${this.config.easing}`
            this.sliderFrame.current.style.webkitTransition = value
            this.sliderFrame.current.style.transition = value

            const currentSlide = this.config.loop ? this.currentSlide + this.config.perPage : this.currentSlide
            const currentOffset = currentSlide * (this.selectorWidth / this.config.perPage)
            const dragOffset = (this.drag.endX - this.drag.startX)
            const offset = this.config.rtl ? currentOffset + dragOffset : currentOffset - dragOffset
            this.sliderFrame.current.style[this.transformProperty] = `translate3d(${(this.config.rtl ? 1 : -1) * offset}px,0,0)` 
        }
    }

    /**
     * mouseleave event handler
     */
    mouseleaveHandler = e => {
        if (this.pointerDown) {
            this.pointerDown = false
            this.selector.current.style.cursor = '-webkit-grab'
            this.drag.endX = e.pageX
            this.drag.preventClick = false
            this.enableTransition()
            this.updateAfterDrag()
            this.clearDrag()
        }
    }

    /**
     * click event handler
     */
    clickHandler = e => {
        // if the dragged element is a link
        // prevent browsers from following the link
        if (this.drag.preventClick) {
            e.preventDefault()
        }
        this.drag.preventClick = false
    }

    /**
     * Attaches listeners to required events
     */
    attachEvents() {
        // Resize element on window resize
        window.addEventListener('resize', this.resizeHandler)

        // If element is draggable / swipable, add event handler
        if (this.config.draggable) {
            // Keep track pointer hold and dragging distance
            this.pointerDown = false
            this.drag = {
                startX: 0,
                endX: 0,
                startY: 0,
                letItGo: null,
                preventClick: false
            }

            // Touch events
            this.selector.current.addEventListener('touchstart', this.touchstartHandler)
            this.selector.current.addEventListener('touchend', this.touchendHandler)
            this.selector.current.addEventListener('touchmove', this.touchmoveHandler)

            // Mouse events
            this.selector.current.addEventListener('mousedown', this.mousedownHandler)
            this.selector.current.addEventListener('mouseup', this.mouseupHandler)
            this.selector.current.addEventListener('mouseleave', this.mouseleaveHandler)
            this.selector.current.addEventListener('mousemove', this.mousemoveHandler)

            // Click
            this.selector.current.addEventListener('click', this.clickHandler)
        }
    }

    /**
     * Detaches listeners from required events
     */
    detachEvents() {
        window.removeEventListener('resize', this.resizeHandler)
        this.selector.current.removeEventListener('touchstart', this.touchstartHandler)
        this.selector.current.removeEventListener('touchend', this.touchendHandler)
        this.selector.current.removeEventListener('touchmove', this.touchmoveHandler)

        // Mouse events
        this.selector.current.removeEventListener('mousedown', this.mousedownHandler)
        this.selector.current.removeEventListener('mouseup', this.mouseupHandler)
        this.selector.current.removeEventListener('mouseleave', this.mouseleaveHandler)
        this.selector.current.removeEventListener('mousemove', this.mousemoveHandler)

        // Click
        this.selector.current.removeEventListener('click', this.clickHandler)
    }
    static webkitOrNot() {
        const style = document.documentElement.style
        if (typeof style.transform === 'string') {
            return 'transform'
        }
        return 'WebkitTransform'
    }
    destroy() {
        this.detachEvents()
    }
    renderItems() {
        const ret = []
        const children = React.Children.toArray(this.props.children)
        const itemWidth = this.config.loop ?
                            `${100 / (children.length + (this.config.perPage * 2))}%` :
                            `${100 / children.length}%`
        const renderChild = child => (
            <div key={child.src} className={styles.item} style={{width: itemWidth}} >
                {child}
            </div>
        )
        if (this.config.loop) {
            for (let i=children.length - this.config.perPage; i < children.length; i++) {
                ret.push(renderChild(children[i]))
            }
        }
        for (let i=0; i < children.length; i++) {
            ret.push(renderChild(children[i]))
        }
        if (this.config.loop) {
            for (let i=0; i < this.config.perPage; i++) {
                ret.push(renderChild(children[i]))
            }
        }
        return ret
    }
    render() {
        const childrenLen = React.Children.count(this.props.children)
        const itemsToBuild = this.config.loop ?
                                childrenLen + (2 * this.config.perPage) :
                                childrenLen
        return (
            <div ref={this.selector} className={cx('selector', {draggable: this.config.draggable})}>
                <div
                    ref={this.sliderFrame}
                    className={styles.slider}
                    style={{
                        width: `${(100 / this.config.perPage) * itemsToBuild}%`
                    }}
                >
                    {this.renderItems()}
                </div>
            </div>
        )
    }
}
export default ReactSiema
