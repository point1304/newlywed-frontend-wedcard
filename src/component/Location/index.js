import React from 'react'
import loadable from '@loadable/component'

const Location = loadable(() => import(/* webpackChunkName: "navermap" */ './LocationWrapped'))
export default function(props) {
    return (
        <Location {...props} fallback={<div>Loading</div>} />
    )
}
