import React from 'react'
import NaverMap, {Marker, Overlay} from 'react-naver-map'
import styles from './index.module.scss'

const Location = (props) => {
    return (
        <NaverMap
            clientId='zcrtjcenwb'
            ncp
            style={{width: '100%', height: '100%'}}
            initialPosition={{lat: props.lat, lng: props.lng}}
            initialZoom={14}
        >
            <Marker
                lat={props.lat}
                lng={props.lng}
                shape={{coords: [0,12, 12,0, 24,12, 12,32, 0,12], type: 'poly'}}
            />
            <Overlay
                lat={props.lat}
                lng={props.lng}
                zIndex={20}
            >
                <div className={styles.overlay}>
                    {props.name}
                </div>
            </Overlay>
        </NaverMap>
    )
}
export default Location
