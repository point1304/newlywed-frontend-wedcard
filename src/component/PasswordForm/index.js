import React, { useRef } from 'react'
import styles from './index.module.scss'

export default function(props) {
    const formRef = useRef()
    const handleSubmit = e => {
        e.stopPropagation()
        e.preventDefault()
        props.onSubmit && props.onSubmit(e)
    }
    const handleClose = e => {
        e.stopPropagation()
        formRef.current.querySelector('input').value = ''
        props.onClose && props.onClose()
    }
    return (
        <div
            className={styles._}
            onClick={e => {e.stopPropagation()}}
        >
            <div className={`${styles.msg} ${styles.content}`}>메시지 작성시 입력한 패스워드를 입력하세요</div>
            <div className={`${styles.err} ${styles.content}`}>{props.error && props.error.message}</div>
            <form ref={formRef} onSubmit={handleSubmit}>
                <input className={styles.input} type="password" name="password" autoComplete="false" value={props.initialize ? '' : undefined} />
                <input className={`${styles.btn} ${styles.l}`} type="button" value="닫기" onClick={handleClose} />
                <input className={`${styles.btn} ${styles.r}`} type="submit" value="제출" />
            </form>
        </div>
    )
}
