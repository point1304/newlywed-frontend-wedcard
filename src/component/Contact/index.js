import React from 'react'
import { PhoneIcon, SMSIcon } from '../Icon'
import styles from './index.module.scss'
import classnames from 'classnames/bind'
const cx = classnames.bind(styles)

const renderIcon = (Icon, href) => (
    <span className={styles.btn}><Icon href={href} /></span>
)
export default function(props) {
    return (
        <div className={cx('_', {v: props.v})}>
            <span>{props.name}</span>
            <span>{'\u00A0\u00A0\u00A0'}</span>
            <span style={{display: 'flex', alignItems: 'center'}}> 
                {renderIcon(PhoneIcon, `tel://${props.phoneNumber}`)}
                {'\u00A0\u00A0|\u00A0\u00A0'}
                {renderIcon(SMSIcon, `sms://${props.phoneNumber}`)}
            </span>
        </div>
    )
}
