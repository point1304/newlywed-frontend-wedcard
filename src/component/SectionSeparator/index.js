import React from 'react'
import styles from './index.module.scss'

export default function({title, imageUrl, width, height}) {
    return (
        <div
            className={styles._}
            style={{
                background: `center / cover no-repeat url(${imageUrl})`,
                paddingTop: `${(height / width) * 100}%`
            }}
        >
            <span className={styles.title}>{title}</span>
        </div>
    )
}