import React from 'react'
import Contact from '../Contact'
import styles from './index.module.scss'

export default function(props) {
    return (
        <div className={styles._}>
            <div className={styles.couple}>
                <Contact
                    name={'신랑에게 축하인사'}
                    phoneNumber={props.groom.phoneNumber}
                />
                <Contact
                    name={'신부에게 축하인사'}
                    phoneNumber={props.bride.phoneNumber}
                />
            </div>
            <div className={styles.parents}>
                <div className={styles.header}>혼주에게 연락하기</div>
                <div className={styles.parent}>
                    <span className={styles.whose}>신랑측 혼주</span>
                    <Contact
                        name={`아버지 ${props.fatherOfGroom.name}`}
                        phoneNumber={props.fatherOfGroom.phoneNumber}
                        v
                    />
                    <Contact
                        name={`어머니 ${props.motherOfGroom.name}`}
                        phoneNumber={props.motherOfGroom.phoneNumber}
                        v
                    />
                </div>
                <div className={styles.parent}>
                    <span className={styles.whose}>신부측 혼주</span>
                    <Contact
                        name={`아버지 ${props.fatherOfBride.name}`}
                        phoneNumber={props.fatherOfBride.phoneNumber}
                        v
                    />
                    <Contact
                        name={`어머니 ${props.motherOfBride.name}`}
                        phoneNumber={props.motherOfBride.phoneNumber}
                        v
                    />
                </div>
            </div>
        </div>
    )
}
