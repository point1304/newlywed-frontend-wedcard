import React from 'react'
import Form from '../Form'
import CSSTransitionGroup from 'react-transition-group/CSSTransition'
import styles from './index.module.scss'

export default function(props) {
    return (
        <CSSTransitionGroup
            in={props.show}
            classNames={{
                enter: styles.enter,
                enterActive: styles.enterActive,
                enterDone: styles.enterDone,
                exit: styles.exit,
                exitActive: styles.exitActive,
                exitDone: styles.exitDone
            }}
            timeout={310}
        >
            <div className={styles._}>
                <div style={{maxWidth: '600px', margin: 'auto'}}>
                    <div className={styles.to}>
                        {`@${props.commentTo}에게 보내는 메시지`}
                    </div>
                    <Form {...props} />
                </div>
            </div>
        </CSSTransitionGroup>
    )
}
