import React from 'react'
import classNames from 'classnames/bind'
import styles from './index.module.scss'
const cx = classNames.bind(styles)

class Image extends React.PureComponent {
    constructor(props) {
        super(props)
        this.state = {isLoaded: false}
        this.hasSize = this.props.width && this.props.height
    }
    onLoad = () => {
        this.setState({isLoaded: true})
    }
    render() {
        return (
            <div className={styles.wrap} style={this.props.style}>
                <div className={cx('placeHolder', {isLoaded: this.state.isLoaded})} style={{
                    paddingTop: this.hasSize && `${(this.props.height / this.props.width) * 100}%`,
                    height: this.hasSize && 0,
                    position: this.hasSize && !this.state.isLoaded && 'relative'
                    }}/>
                <img
                    className={cx(
                        this.props.cover ? 'cover' :
                        this.props.contain ? 'contain': null
                    )}
                    src={this.props.src}
                    alt=''
                    onLoad={this.onLoad}
                    style={{ borderRadius: !this.state.isLoaded && '10px' }}
                    data-object-fit={
                        this.props.cover ? 'cover' :
                        this.props.contain ? 'contain': null
                    }
                />
            </div>
        )
    }
}
export default Image
