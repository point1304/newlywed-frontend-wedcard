import React from 'react'
import CSSTransitionGroup from 'react-transition-group/CSSTransition'
import styles from './index.module.scss'

export default function(props) {
    const clickHandler = e => {
        e.stopPropagation()
        props.onClick && props.onClick(e)
    }
    return (
        <CSSTransitionGroup
            in={props.show}
            classNames={{
                enter: styles.enter,
                enterActive: styles.enterActive,
                enterDone: styles.enterDone,
                exit: styles.exit,
                exitActive: styles.exitActive,
            }}
            timeout={310}
        >
            <div
                className={styles._}
                onClick={clickHandler}
            >
                {props.children}
            </div>
        </CSSTransitionGroup>
    )
}