import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import NavBar from './component/NavBar'
import MainPage from './page/MainPage'

import styles from './App.module.scss'

const links = [{
	to: '/#contact',
	name: '연락하기',
}, {
	to: '/#gallery',
	name: '갤러리',
}, {
	to: '/#loc',
	name: '장소'
}, {
	to: '/#comments',
	name: '축하메시지'
}]
function App() {
  	return (
		<Router>
			<div className={styles.app}>
				<nav className={styles.nav}>
					<NavBar
						links={links}
						className={styles.nav}
					/>
				</nav>
				<div className={styles.main}>
					<Switch>
						<Route exact path='/' component={MainPage} />
					</Switch>
				</div>
			</div>
		</Router>
  	);
}
export default App