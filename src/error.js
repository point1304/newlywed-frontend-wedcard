export class RequestFailedError extends Error {
    constructor(status, ...params) {
        super(...params)

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, RequestFailedError)
        }

        this.status  = status
        this.name    = 'RequestFailedError'
    }
}