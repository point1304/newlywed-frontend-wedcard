import 'react-app-polyfill/ie11'
import 'react-app-polyfill/stable'
import 'objectFitPolyfill'
import scrollIntoViewPolyfill from 'smooth-scroll-into-view-if-needed'

const UA = navigator.userAgent,
    isIE = window.document.documentMode === undefined  ? false : true,
    isEdge = /Edge/.test(UA),
    isWebkit = /\b(iPad|iPhone|iPod)\b/.test(UA) &&
                /WebKit/.test(UA) && !isEdge && !window.MSStream;

(function() {
    if (isWebkit || isEdge || isIE) {
        const Element = window.HTMLElement || window.Element
        Element.prototype.scrollIntoView = function(option) {
            let op = {behavior: 'auto', block: 'start', inline: 'nearest'}
            if (option === false) op = {...op, block: 'end', inline: 'nearest'}
            else op = {...op, ...option}
            scrollIntoViewPolyfill(this, op)
        }
    }
})();
/** POLYFILLS */
// Smoothscroll polyfill for iOS webkit
// convert rAF into setTimeout implementation to fix the iOS webkit bug.
// rAF stack blocks when a page is retrieved from BFcache.
(function() {
    window.onpageshow = e => {
        if (e.persisted) {
            if (isWebkit) {
                var lastTime = 0
                window.requestAnimationFrame = function(callback, element) {
                    var currTime = performance.now()
                    var timeToCall = Math.max(0, 16 - (currTime - lastTime))
                    var id = window.setTimeout(function() { callback(currTime + timeToCall) }, 
                      timeToCall)
                    lastTime = currTime + timeToCall;
                    return id
                }
                window.cancelAnimationFrame = function(id) {
                    clearTimeout(id)
                }
            }
        }
    }
})();
