import { urlencode } from './util'
import { callAPI, deleteAPI, postAPI } from './util/api'
import { normalize, schema } from 'normalizr'

const API_ROOT = 'https://api.newlywed.me/latest'

const TAG = window.location.hostname.split('.').length === 3 ?
                window.location.hostname.split('.')[0] : 'love'

export const sampleGetApi = () => callAPI(`/sample?${urlencode({this: 'that', bits: 'pieces'})}`)
export const somplePostApi = body => postAPI('/sample')
export const fetchWeddingData = () => callAPI(API_ROOT + `/wedCard/${TAG}`)

const comment = new schema.Entity('comments')
const commentFetchResult = {data: [comment]}
comment.define({children: commentFetchResult})
export const fetchComments = async () => {
    const result = await callAPI(API_ROOT + `/wedCard/${TAG}/comment`)
    const ret = normalize(result, commentFetchResult)
    // sort the result in desc order
    ret.result.data.sort().reverse()
    return ret
}
export const postComment = async body => {
    const result = await postAPI(API_ROOT + `/wedCard/${TAG}/comment`, body)
    return normalize(result, comment)
}
export const postReply = async (commentId, body) => {
    const result = await postAPI(API_ROOT + `/wedCard/${TAG}/comment/${commentId}/reply`, body)
    return normalize(result, comment)
}
export const deleteComment = (commentId, body)=> deleteAPI(
    API_ROOT + `/wedCard/${TAG}/comment/${commentId}`,
    body
)
